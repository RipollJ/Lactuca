##############################
### Script Project Lactuca ###
##############################


##############################
# Author: RipollJ
# Created: 2016

##############################
### Environment
rm(list=ls()) # remove environment memory
dev.off() # remove graphics
cat("\014")  # remove console

### Packages
source("https://bioconductor.org/biocLite.R")
ilist <- c("reshape2","ggplot2","pheatmap","edgeR","plyr","knitr","gplots","topGO",
           "GOplot","GO.db","KEGG.db","seqinr","GenomicFeatures","DBI","dplyr","RColorBrewer",
		   "gdata","limma","VennDiagram","factoextra")
if (!require(ilist)) biocLite(ilist)

library(reshape2)
library(ggplot2)
library(pheatmap)
library(edgeR)
library(plyr)
library(knitr)
library(gplots)
library("topGO")
library(GOplot)
library(GO.db)
library(KEGG.db)
library("seqinr")
library(GenomicFeatures)
library(DBI)
library(dplyr)
library(RColorBrewer)
library(gdata)
library(limma)
library(VennDiagram)
library("factoextra")


#################################################################
### Dataset 
#experimental design
groups <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Design.txt", header=T)
groups
groups <- factor(paste(groups$time,groups$condition,sep="."))
design <- model.matrix(~0+groups)
colnames(design) <- levels(groups)
design

#count table
table <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/All_count.txt", header=T, row.names=1)
kable(head(table))

# constants
Project = "Laitue"
m <- melt(table)
p <- ggplot(data = m,aes(x=variable, y=log10(value), group=variable, fill=variable))
p <- p + geom_boxplot() + labs(title = paste(Project,"Expression range", sep=" "))
p <- p + theme(axis.text.x = element_text(angle = 90, hjust = 1))
p
summary(table)

# visualisation des groupes de donnees brutes en Heat-map
pheatmap(cor(log10(table+1)))


#################################################################
#### Analyse differentielle DGE

#creation du fichier de travail
y <- DGEList(counts=table,group=groups)
dim(y)
plotMD(y$counts)

#plot CPM distribution
myPalette <- c(brewer.pal(9, "Set1"), brewer.pal(8, "Set2"))
unfilteredExpr <- cpm(y, log=T)
plotDensities(unfilteredExpr, col=myPalette[1:16], legend="topright")

### Normalisation des donnees et selection selon critre
# Keep genes with least 2 count-per-million reads (cpm) in at least 3 samples (3 replicates)
# valable pour une liste DGE
keep<-rowSums(cpm(y)>2)>=3
table(keep)
y<-y[keep,keep.lib.sizes=FALSE]
dim(y)
plotMD(y$counts)
filteredExpr1 <- cpm(y, log=T)
plotDensities(filteredExpr1, col=myPalette[1:16], legend="topright")
#Note that the filtering does not use knowledge of what treatment corresponds to each sample,
#so the filtering does not bias the subsequent differential expression analysis.
#The TMM normalization is applied to account for the compositional biases:
# normalisation of the samples selon la methode TMM (prenant en compte la profondeur de sequenage)
y <- calcNormFactors(y,method="TMM")
y$samples
dim(y)
filteredExpr <- cpm(y, log=T)
plotDensities(filteredExpr, col=myPalette[1:16], legend="topright")

# Exploration des donnees - graph MDS
plotMDS(y, method="bcv", col=as.numeric(y$samples$group))
legend("topright", as.character(unique(sort(y$samples$group))), col=1:11, pch=20) 
plotDensities(filteredExpr, group=y$samples$group, col=myPalette[1:5],legend="topright")

### Estimer la dispersion du modle pour choisir le modle glm  utiliser (LRT (le plus frequent), QL (courbe type 1/x) ou Treat (si deux traitements))
# dispersion estimation all model in one code
y <- estimateDisp(y, design)

# OR STEP by STEP
# estimate common dispersion
y <- estimateGLMCommonDisp(y, design, verbose=TRUE)
# fitting the model 
y <- estimateGLMTrendedDisp(y, design)
## Loading required package: splines
y <- estimateGLMTagwiseDisp(y, design)
# graphique de la dispersion (La racine carree de la dispersion est le coefficient de variation biologique BCV)
plotBCV(y)

### Test modle lineaire glm 
fit <- glmFit(y,design)
# creation des contrastes pour les comparaisons dans le cas de facteurs multiples
# possibilite de tester les interactions aussi
# The full interaction formula is : design <- model.matrix(~Treat * Time, data=targets)
my.contrasts <- makeContrasts(
  neutral.vs.days27 = day0.neutral - (day2.dark + day2.light + day7.dark + day7.light)/4,
  light.vs.dark = (day2.dark + day7.dark) - (day2.light + day7.light),
  D7D.vs.D0 = day7.dark-day0.neutral,
  D2D.vs.D0 = day2.dark-day0.neutral,
  D7D.vs.D2D = day7.dark-day2.dark,
  D2L.vs.D0 = day2.light-day0.neutral,
  D7L.vs.D0 = day7.light-day0.neutral,
  D2L.vs.D2D = day2.light-day2.dark,
  D7L.vs.D2L = day7.light-day2.light,
  D7L.vs.D7D = day7.light-day7.dark,
  D7L.vs.D2D = day7.light-day2.dark,
  levels=design)

### Effet lumiere
lrt2 <- glmLRT(fit, contrast=my.contrasts[,"D2L.vs.D2D"])

topT2 <-as.data.frame(topTags(lrt2,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT2)
a<-topT2[topT2$FDR <0.01,]
dim(a)
topT2<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT2)
kable(head(topT2, n=50), digits=2)
#visualisation
o2<-order(lrt2$table$PValue)
cpm(y)[o2[1:10],]
#nb de gnes DE  1% FDR
summary(de<-decideTestsDGE(lrt2, adjust.method="BH", p.value=0.01, lfc=1.5))
#graph des gnes DE avec fold-change 2
detags <- rownames(y)[as.logical(de)]
plotSmear(lrt2, de.tags=detags)
abline(h=c(-1.5, 1.5), col="blue")

write.table(topT2, file = "C:/Users/Julie Ripoll/Documents/Light treatment effect results/DGElist light 2 jours.txt", row.names = TRUE)


###################################################
### Annotation du genome DGE

#sink(file="ResultGO light 2 jours.txt", append = F) ### pour export resultat

# ajout colonne gne UP T/F
EG.DE.UP <- topT2$logFC > 0
topT2["UP"]<-EG.DE.UP
str(topT2)

# annotation que gnes UP ou DOWN
sink(file="logFC gene up.txt", append = F)
topT3<-topT2[topT2$UP==TRUE,]
print(topT3)
sink()

sink(file="logFC gene down.txt", append = F)
topT3<-topT2[topT2$UP==FALSE,] # gnes down
print(topT3)
sink()

##
str(topT3)
myInterestingGenes2 <- row.names(topT3)

## ou tous les gnes DE
myInterestingGenes2 <- row.names(topT2)
str(myInterestingGenes2)

## ajout des annotations aux genes DE
geneID2GO <- readMappings(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.topGO.out")
geneNames <- names(geneID2GO)
head(geneNames)

geneList2 <- factor(as.integer(geneNames %in% myInterestingGenes2))
str(geneList2)
names(geneList2) <- geneNames
head(geneList2)
str(geneList2)
summary(geneList2) # nb d'enrichis GO

##############
# Enrichissement GO par boites MF, CC, BP
GOdata_MF2 <- new("topGOdata", ontology = "MF", allGenes = geneList2 , geneSel= myInterestingGenes2, annot = annFUN.gene2GO, gene2GO = geneID2GO)
GOdata_MF2
# test de Fisher pour les analyses sur interesting genes
resultFisher <- runTest(GOdata_MF2, algorithm = "classic", statistic = "fisher")
resultFisher
Fisher_MF2<-GenTable(GOdata_MF2, classicFisher = resultFisher, orderBy = "classicFisher", ranksOf = "classicFisher", topNodes = 50)
Fisher1<-Fisher_MF2[Fisher_MF2$classicFisher<.05,]
print(kable(Fisher1))

GOdata_CC2 <- new("topGOdata", ontology = "CC", allGenes = geneList2 , geneSel= myInterestingGenes2, annot = annFUN.gene2GO, gene2GO = geneID2GO)
resultFisher <- runTest(GOdata_CC2, algorithm = "classic", statistic = "fisher")
resultFisher
Fisher_CC2<-GenTable(GOdata_CC2, classicFisher = resultFisher, orderBy = "classicFisher", ranksOf = "classicFisher", topNodes = 50)
Fisher2<-Fisher_CC2[Fisher_CC2$classicFisher<.05,]
print(kable(Fisher2))


GOdata_BP2 <- new("topGOdata", ontology = "BP", allGenes = geneList2 , geneSel= myInterestingGenes2, annot = annFUN.gene2GO, gene2GO = geneID2GO)
resultFisher <- runTest(GOdata_BP2, algorithm = "classic", statistic = "fisher")
resultFisher
Fisher_BP2<-GenTable(GOdata_BP2, classicFisher = resultFisher, orderBy = "classicFisher", ranksOf = "classicFisher", topNodes = 50)
Fisher3<-Fisher_BP2[Fisher_BP2$classicFisher<.05,]
print(kable(Fisher3))

#sink()

# pour avoir une vue rapide des trois boites
for (ontol in c("BP", "MF", "CC")) {
  GOdata <- new("topGOdata", ontology = ontol, allGenes = geneList2, geneSel= myInterestingGenes2, annot = annFUN.gene2GO, gene2GO = geneID2GO)
  resultFisher <- runTest(GOdata, algorithm = "classic", statistic = "fisher")
  print(resultFisher)  
  allRes <- GenTable(GOdata, classic = resultFisher, topNodes = 50)
  print(allRes)
  #showSigOfNodes(GOdata, score(resultFisher), firstSigNodes = 5, useInfo = "all")
  
}
##############################################
# creation d'un fichier complet avec toutes les annotations GO pour export (si besoin)
Fisher1["category"]<-"MF"
Fisher2["category"]<-"CC"
Fisher3["category"]<-"BP"
allGO2<-rbind(Fisher1, Fisher2, Fisher3)

write.table(allGO2, file = "C:/Users/Julie Ripoll/Documents/Light treatment effect results/ALLGO_light_7 vs J0.txt", row.names = TRUE)


##########################################################################################################
### Travail sur le level 2 GO
###########################################
# recuperation liste d'une des sous-categories GO
# liste de tous les gnes annotes
#a<-genes(GOdata_CC2)
#head(a)
numGenes(GOdata_MF2)

#sink(file="Terms genes DN MF2.txt", append = F) ### pour export resultat
sg<-sigGenes(GOdata_MF2) # genes significatifs
str(sg)
### recuperation tous les gnes up ou down avec leur boite GO
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/DR_annotations.txt", header=F, sep="\t")
test = sg %in% table_anot[,1]#test pour savoir si le gne est annote
Res = data.frame(sg, test)
head(Res)
kable(table_anot[table_anot[,1] %in% sg,])
GeneUP1<-table_anot[table_anot[,1] %in% sg,] # mettre le nombre correspondant  la categorie traitee

#sink(file="Terms genes DN MF2.txt", append = F) ### pour export resultat
sg<-sigGenes(GOdata_CC2) # genes significatifs
str(sg)
### recuperation tous les gnes up ou down avec leur boite GO
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/DR_annotations.txt", header=F, sep="\t")
test = sg %in% table_anot[,1]#test pour savoir si le gne est annote
Res = data.frame(sg, test)
head(Res)
kable(table_anot[table_anot[,1] %in% sg,])
GeneUP2<-table_anot[table_anot[,1] %in% sg,] # mettre le nombre correspondant  la categorie traitee

#sink(file="Terms genes DN MF2.txt", append = F) ### pour export resultat
sg<-sigGenes(GOdata_BP2) # genes significatifs
str(sg)
### recuperation tous les gnes up ou down avec leur boite GO
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/DR_annotations.txt", header=F, sep="\t")
test = sg %in% table_anot[,1]#test pour savoir si le gne est annote
Res = data.frame(sg, test)
head(Res)
kable(table_anot[table_anot[,1] %in% sg,])
GeneUP3<-table_anot[table_anot[,1] %in% sg,] # mettre le nombre correspondant  la categorie traitee

#sortie avec tous les resultats
GeneUP1["category"]<-"MF"
GeneUP2["category"]<-"CC"
GeneUP3["category"]<-"BP"
# fusion des tableaux pour voir les gnes qui sont dans plusieurs categories (BP, MF, CC)
merge1<-merge(GeneUP1, GeneUP2, by="V1", all=T)
GeneUP<-merge(merge1,GeneUP3, by="V1", all=T) 
head(GeneUP)
GeneUP$category<-with(GeneUP, paste(category.x, category.y, category, sep="_"))
GeneUP$gene_name<-with(GeneUP, paste(V2.x, V2.y, V2, sep="%"))
names(GeneUP)
GeneUP<-GeneUP[,-c(2,3,4,5,6)]
names(GeneUP)
colnames(GeneUP) <- c('Lsat', 'GO','Gene_name')
head(GeneUP)
name<-GeneUP$Gene_name
name2<-gsub("%NA"," ",name) # remplace la valeur 1 par la valeur 2
name2<-gsub("NA%"," ",name2) # juste pour nettoyer les identifiants qui sont merges
name2<-gsub("NA %"," ",name2)
GeneUP$Gene_name<-name2
head(GeneUP)
# write.table(GeneUP, file = "C:/Users/Julie Ripoll/Documents/Light treatment effect results/ALL_Gene_light_7 vs J7 dark.txt", row.names = TRUE, sep="\t") # reste  separer les noms en double dans excel en separant selon '%'

# export liste gnes up ou down avec logFC (si besoin - etape intermediaire)
tableUP <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/GeneUP.txt", header=T, sep ="\t")
#tableDN <- read.table("C:/Users/Julie Ripoll/Documents/Browning effect results/GeneDown.txt", header=T, sep ="\t")
topT3 <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/logFC gene up.txt", header=T, sep ="\t")
#topT3 <- read.table("C:/Users/Julie Ripoll/Documents/Browning effect results/logFC gene down.txt", header=T, sep ="\t")
UPlog<-merge(tableDN, topT3, by="Lsat", all=T)
write.table(UPlog,"UPlog.txt",dec=".",quote=FALSE,row.names=FALSE) # tableau final avec les noms des gnes et les logFC


### Recuperations des infos des boites GO level 2 (terms et definition)
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.topGO.out", header=F, sep="\t")
test = sg %in% table_anot[,1]#test pour savoir si le gne possde des GO:ID (normalement oui !)
head(test)
list_ok=table_anot[table_anot[,1] %in% sg,]#va chercher les genes de la "list" dans la table d'annotation GO
kable(list_ok)

# Attention, list_ok contient des "factors" : nom des genes et GO:ID, il faut les convertir en character
list_ok <- data.frame(lapply(list_ok, as.character), stringsAsFactors=FALSE) # Convert data.frame columns from factors to characters / sinon impossible d'aapliquer la function strsplit
V_ID <- with(list_ok, strsplit(V2, ", ")) #converti les chaines de la colone 2  en vecteur pour ensuite interroger la bdd GO
names(V_ID) <- with(list_ok, as.character(V1)) # recupere les noms des gnes

terms <- lapply(V_ID, function(x, db) select(db, x, "TERM", "GOID"), db = GO.db)# interroge la bdd, donne les termes GO
head(terms)

Definition <- lapply(V_ID, function(x, db) select(db, x, "DEFINITION", "GOID"), db = GO.db)# interroge la bdd, donne les definitions
head(Definition)

#Ontology <- lapply(V_ID, function(x, db) select(db, x, "ONTOLOGY", "GOID"), db = GO.db)# interroge la bdd pour sortir les boites GO
#head(Ontology)

#sink()

## Preparation des fichiers d'export avec toutes les infos
# fichier terms en format colonne pour export excel
melt_terms <- melt(terms) # recuperation des termes
names(melt_terms)
colnames(melt_terms) <- c('GO.ID', 'Term','Lsat')
str(melt_terms)
head(melt_terms)

melt_definition <- melt(Definition) # recuperation des definitions
names(melt_definition)
colnames(melt_definition) <- c('GO.ID', 'Definition','Lsat')
str(melt_definition)
head(melt_definition)

## recuperation des logCPM par condition (si besoin)
logcpm<-cpm(y, prior.count = 2, log=T)
head(logcpm)
colnames(logcpm)=c("day0","day0","day0","day2D","day2D","day2D","day7D","day7D","day7D","day2L","day2L","day2L","day7L","day7L","day7L")
mt = t(logcpm)
logcpmF<-sapply(by(mt,rownames(mt),colMeans),identity)
head(logcpmF)


## recuperation des CPM par condition
CPM<-cpm(y, prior.count = 2, log=F)
head(CPM)
colnames(CPM)=c("day0","day0","day0","day2D","day2D","day2D","day7D","day7D","day7D","day2L","day2L","day2L","day7L","day7L","day7L")
head(CPM)
mt = t(CPM) # transposition pour les calculs de moyenne
cpmF_Mean<-sapply(by(mt,rownames(mt),colMeans),identity)
head(cpmF_Mean)
# pour recuperer les ecart-types SD
colSd <- function (x, na.rm=FALSE) apply(X=x, MARGIN=2, FUN=sd, na.rm=na.rm)
cpmF_SD<-sapply(by(mt,rownames(mt),colSd),identity)
head(cpmF_SD)
# merge des deux tableaux de donnees moyenne et SD
cpmF<-merge(cpmF_Mean, cpmF_SD, by="row.names")
head(cpmF)

# export resultats (si besoin - etape intermediaire)
options(max.print=100000)
topT<-rownames(topT2)
topcpmF<-cpmF[cpmF[,1] %in% topT,]
str(topcpmF)
sink(file="CPM ALL Gene Light 7 vs J2 dark.txt", append = F)
print(topcpmF)
sink()

# fusion des data.frames pour avoir le fichier final
topT2$Lsat<-rownames(topT2)
TopData<-merge(topT2,cpmF, by='Lsat',all=F)
str(TopData)
# ajout des terms ou definition (fusion possible sur excel aprs)
Data_2L<-merge(TopData,GeneUP, by='Lsat',all=T)
str(Data_2L)
write.table(Data_2L,"Data_7L vs D2D_ALL.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE) # tableau final avec les noms des gnes et les logFC
#
TotalData2<-merge(GeneUP,melt_definition,by="Lsat",all=T)
str(TotalData2)
TotalData<-merge(TotalData2,TopData,by="Lsat",all=T)
str(TotalData)
head(TotalData)
write.table(TotalData,"TotalData_Def_7D vs J2D_ALL.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE) # tableau final avec les noms des gnes et les logFC
#
TotalData3<-merge(GeneUP,melt_terms,by="Lsat",all=T)
str(TotalData3)
TotalData<-merge(TotalData3,TopData,by="Lsat",all=T)
str(TotalData)
head(TotalData)
write.table(TotalData,"TotalData_Term_7D vs 2D_ALL.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE) # tableau final avec les noms des gnes et les logFC


##############################################################################################
#######
# liste des boites GO 
Godata<-updateGenes(GOdata_MF2,geneList2)
Godata
ug <- usedGO(Godata)
head(ug)
str(ug) # nombre de boites GO
#list genes annotated par boites GO
sel.terms <- sample(usedGO(Godata), 1089) #taille exacte du nb de boites GO  mettre
num.ann.genes <- countGenesInTerm(Godata, sel.terms) ## the number of annotated genes
num.ann.genes
ann.genes <- genesInTerm(Godata, sel.terms) ## get the annotations
head(ann.genes)
#score d'annotation
ann.score <- scoresInTerm(Godata, sel.terms)
head(ann.score)
ann.score <- scoresInTerm(Godata, sel.terms, use.names = TRUE)
head(ann.score)
## to summarise the statistics
stat<-termStat(Godata, sel.terms)
write.table(stat, file = "C:/Users/Julie Ripoll/Documents/Light treatment effect results/Stat Gene UP.txt", row.names = TRUE)

# info sur les boites GO
enriched.GO=allGO2$GO.ID[p.adjust(allGO2$classicFisher, method="BH")<.01]
str(enriched.GO)
for(go in enriched.GO[1:50]){
  print(GOTERM[[go]])
  cat("--------------------------------------\n")
}


########################################################################################################
### KEGG
#creation fichier export pour site KEGG des gnes DE
gene2KEGG <- read.table(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.loci.all.KEGG_pathways3",header=F)
head(gene2KEGG)
dim(gene2KEGG)
str(gene2KEGG)

str(myInterestingGenes2)

names(gene2KEGG) <- c("gene","ko","map")
dim(gene2KEGG[gene2KEGG$gene %in% myInterestingGenes2, ])
KEGGlistGene<-gene2KEGG[gene2KEGG$gene %in% myInterestingGenes2, ]
str(KEGGlistGene)
write.table(KEGGlistGene,"KEGGlistGene_2L vs 2D 1 005.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE) # fichier pour site web KEGG


## juste pour avoir les correspondances nom des gnes (si besoin - etape intermediaire)
names(gene2KEGG) <- c("Lsat","ko","map")
str(gene2KEGG)
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/DR_annotations.txt", header=F, sep="\t")
names(table_anot) <- c("Lsat","Gene_name")
str(table_anot)
joinData<-merge(gene2KEGG, table_anot)
str(joinData)

dim(joinData[joinData$Lsat %in% myInterestingGenes2, ])
KEGGlistGene<-joinData[joinData$Lsat %in% myInterestingGenes2, ]
str(KEGGlistGene)
write.table(KEGGlistGene,"KEGG_Gene.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)


##########################################################################################################
### Extraction d'une sequence
######################################################

# lecture du fasta de reference
#install.packages("seqinr")
#setwd("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.ScaffoldsSequence.all.fasta")
fastafile<- read.fasta(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.ScaffoldsSequence.all.fasta/Lsat.1.v5.ScaffoldsSequence.all.fasta",
                       seqtype = "DNA",as.string = TRUE, set.attributes = FALSE) # trs long
str(fastafile)

### GFF3: lecture du gff, fichier d'alignements
#source("https://bioconductor.org/biocLite.R")
#biocLite("GenomicRanges")
#biocLite("rtracklayer")
#biocLite("GenomicFeatures")
#library(GenomicRanges)
#library(rtracklayer)
#gffFile<-readGFF(filepath="C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.gff3/Lsat.1.v5.gene.allTranscript.gff3", version=1)
#str(gffFile)


# il n'est pas necessaire d'avoir charger le fasta pour ce qui suit:
txdb <- makeTxDbFromGFF("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.gff3/Lsat.1.v5.gene.allTranscript.gff3", 
                        format="gff3")
txdb
genes <- genes(txdb)
#write.table(as.data.frame(genes)[,-4], file="Just_genes.txt", sep="\t") # export 
genes["Lsat_1_v5_gn_7_3921"] # donne la ligne correspondante avec les ID, seqnames pour ID du fichier fasta

genes["Lsat_1_v5_gn_2_106520"]# PAL
genes["Lsat_1_v5_gn_2_106540"]# PAL
genes["Lsat_1_v5_gn_6_31000"]# PAL
genes["Lsat_1_v5_gn_6_31080"]# PAL
genes["Lsat_1_v5_gn_0_6400"]# PPO
genes["Lsat_1_v5_gn_4_54780"]# PPO
genes["Lsat_1_v5_gn_5_119940"]# PPO
genes["Lsat_1_v5_gn_7_100781"]# PPO
genes["Lsat_1_v5_gn_9_102220"]# PPO
genes["Lsat_1_v5_gn_9_102320"]# PPO

# recup de la sequence
fastafile["Lsat_1_v5_g_2_2141"] # visualisation
options(max.print=10000000) # aggrandie la limite d'impression dans le fichier texte
setwd("C:/Users/Julie Ripoll/Documents/Lactuca project")
sink(file="Lsat_1_v5_g_2_2141.txt", append = F) # debut enregistrement
print(fastafile["Lsat_1_v5_g_2_2141"]) # export de la sequence complete dans un fichier texte
sink() # fin enregistrement

length(fastafile["Lsat_1_v5_g_4_673308"])
nchar(fastafile["Lsat_1_v5_g_4_673308"])
str(fastafile["Lsat_1_v5_g_4_673308"])

sink(file="Lsat_1_v5_gn_9_1560.txt", append = F) 
print(substr(fastafile["Lsat_1_v5_g_9_223"],1008581,1010500))# recupere la sequence pour blast
sink()

############
# pour voir le nb de chromosome si defini
head(seqlevels(txdb))# pas le cas sur notre jeu de donnees, besoin de specifier un genome de REF

############
### recup sequence proteique
Test<-substr(fastafile["Lsat_1_v5_g_9_223"],1008581,1010500)
Test2<-as.SeqFastadna(Test, name="Test", Annot = "blablabla")
# lecture des codons
# Biologically correct, only one stop codon at the end
getTrans(Test2, frame=3, sens="R", numcode=6, NAstring="X", ambiguous=T)


###################################################
# extraire des donnees par mot cles du fichier R
#####

# note: il est plus interessant de passer par Notepad ++ pour a (gain de temps)
gene<-as.data.frame(y$counts)
gene$Lsat<-rownames(gene)
str(gene)
table_anot <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/DR_annotations.txt", header=F, sep="\t")
names(table_anot) <- c("Lsat","Gene_name")
str(table_anot)

joinData<-merge(gene, table_anot)
str(joinData)
write.table(joinData,"All Gene annotated Swiss prot.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

joinData$Gene_name[grep("Riboflavin",joinData$Gene_name)] # liste
joinData$Gene_name[grep("Phenylalanine",joinData$Gene_name)] # liste
###########################################################################################################################


##############################################################################################
# recherche de similarite et dissimilarite entre deux data frames
##

# fonctionne moins bien que la recuperation des listes aprs un diagramme de Venn
Data_2L <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/Fichier D2L vs D2D/Data_2D.txt",header=T, sep="\t")
head(Data_2L)
colnames(Data_2L)=c("Lsat","logFCD2","logCPMD2","LRD2","PValueD2","FDR2"," UPD2","day0_MD2","day2_MD2","day0_sdD2","day2_sdD2","GOD2","Gene_name")
str(Data_2L)
Data_7L <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/Fichier D7L vs D2L/Data_7D.txt", header=T, sep="\t")
head(Data_7L)
colnames(Data_7L)=c("Lsat","logFCD7","logCPMD7","LRD7","PValueD7","FDR7"," UPD7","day0_MD7","day2_MD7","day0_sdD7","day2_sdD7","GOD7","Gene_name")
str(Data_7L)

#test
myInterestingGenes <- Data_2L$Lsat
myInterestingGenes2 <- Data_7L$Lsat
str(myInterestingGenes)
str(myInterestingGenes2)
geneListCom <- factor(as.integer(myInterestingGenes2 %in% myInterestingGenes))
summary(geneListCom) # nb Lsat communs et differents

# liste gnes DE communs entre Data_2D et Data_7D
com2<-Data_2L$Lsat
Diff<-Data_7L[Data_7L[,1] %in% com2,]
str(Diff)
write.table(Diff,"Com term D2 vs D7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)
#pour avoir les valeurs D2 et D7
comic<-merge(Data_2D, Data_7D, by="Lsat", all=F)
str(comic)
write.table(comic,"Com term D2 vs D7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

# liste gnes DE de D7 non retrouve  D2
com2<-Data_2L$Lsat
Diff<-Data_7L[!(Data_7L[,1] %in% com2),]
str(Diff)
write.table(Diff,"Diff D7 non retrouve  D2.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

# liste des genes DE de D2 non retrouve  D7
com2<-Data_7L$Lsat
Diff<-Data_2L[!(Data_2L[,1] %in% com2),]
str(Diff)
write.table(Diff,"Diff D2 non retrouve  D7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)


##############################################
#### Graphiques specifiques ##################
##############################################

###################################################
# graph heatmap tous les genes (hors analyse DGE)
###

y<-estimateDisp(y, design,robust=TRUE)
logCPM<-cpm(y,prior.count=2, log=T)
o<- order(lrt1$table$PValue)
logCPM<-logCPM[o[1:10],]
rownames(logCPM)<-y$genes$Symbol
colnames(logCPM)<-paste(y$samples$group,1:2,sep="-")
logCPM<-t(scale(t(logCPM)))

logCPM <- cpm(y,prior.count=2,log=TRUE)
rownames(logCPM)<-y$genes$Symbol  
colnames(logCPM)<-paste(y$samples$group,1:2,sep="-")
tr<-glmTreat(fit,contrast=my.contrasts[,"day2dark.vs.day2light"],lfc=log2(1.5))
topTags(tr)
o<-order(tr$table$PValue)
logCPM<-logCPM[o[1:50],]
logCPM<-t(scale(t(logCPM)))

col.pan<-colorpanel(100,"blue","white","red")
heatmap.2(logCPM,col=col.pan,Rowv=T,scale="none",trace="none",dendrogram="both",cexRow=1,cexCol=1.4)

col.pan<-colorpanel(100,"blue","white","red")
heatmap.2(logCPM,col=col.pan,Rowv=TRUE,scale="none",key=TRUE,
          trace="none",dendrogram="both",cexRow=1,cexCol=1.4,
          density.info="none",margin=c(10,9),lhei=c(2,10),lwid=c(2,6))


###########################################
## Graph Whisker
####

# A utiliser apres avoir creer le fichier y (et l'avoir normalise et filtre)
# les 10 premiers gene DE selon un contraste donne
plot_whisker_profile_vector <- function(contig_names){
  for (i in 1:length(contig_names)){
    z <- as.data.frame(y$counts[contig_names[i],])
    #print(z)
    names(z)[1] <- "value"
    z$row <- row.names(z)
    #print(z)
    z$class <- factor(substr(row.names(z), 1, 9), levels = c("day0.neut","day2.dark","day7.dark","day2.ligh","day7.ligh"))
    print(ggplot(data=z, aes(factor(class), value , fill=class)) + geom_boxplot() +  xlab("Groups") + ggtitle(contig_names[i])  + geom_text(aes(label=row) ,position=position_jitter(width=0.5, height=0.0)))
    #Sys.sleep(10)
  }
}
plot_whisker_profile_vector(row.names(topT2)[1:10])

# Pour un gne en particulier: 
plot_whisker_profile_vector <- function(contig_names){
  for (i in 1:length(contig_names)){
    z <- as.data.frame(y$counts[contig_names[i],])
    #print(z)
    names(z)[1] <- "value"
    z$row <- row.names(z)
    #print(z)
    z$class <- factor(substr(row.names(z), 1, 9), levels = c("day0.neut","day2.dark","day7.dark","day2.ligh","day7.ligh"))
    print(ggplot(data=z, aes(factor(class), value , fill=class)) + geom_boxplot() +  xlab("Groups") + ggtitle(contig_names[i]) 
          + geom_text(aes(label=row) ,position=position_jitter(width=0.5, height=0.0)))
    #Sys.sleep(10)
  }
}
#x11(width=250, height=250)
 
# flux cyclique
plot_whisker_profile_vector("Lsat_1_v5_gn_3_73300")

# catalase
plot_whisker_profile_vector("Lsat_1_v5_gn_8_26440")
plot_whisker_profile_vector("Lsat_1_v5_gn_8_26460")

# SOD
plot_whisker_profile_vector("Lsat_1_v5_gn_6_43340")
plot_whisker_profile_vector("Lsat_1_v5_gn_8_10740")

# zeaxanthin et xanthonin (carotenoid low light)
plot_whisker_profile_vector("Lsat_1_v5_gn_2_105900")
plot_whisker_profile_vector("Lsat_1_v5_gn_3_90761")
plot_whisker_profile_vector("Lsat_1_v5_gn_9_111181")

# WRKY
plot_whisker_profile_vector("Lsat_1_v5_gn_7_60801") # WRKY50
plot_whisker_profile_vector("Lsat_1_v5_gn_4_35321") # WRKY22
plot_whisker_profile_vector("Lsat_1_v5_gn_7_4000") # WRKY22
plot_whisker_profile_vector("Lsat_1_v5_gn_9_39460") # WRKY70
plot_whisker_profile_vector("Lsat_1_v5_gn_9_60201") # WRKY70


########################################################
### barplot des count et % selon les boites GO
#############

## definition des fonctions avant plot (topGO)
getTermsDefinition <- function(whichTerms, ontology, numChar = 20, 
                               multipLines = FALSE) {
  qTerms <- paste(paste("'", whichTerms, "'", sep = ""), collapse = ",")
  retVal <- dbGetQuery(GO_dbconn(), 
                       paste("SELECT term, go_id FROM go_term WHERE ontology IN",
                             "('", ontology, "') AND go_id IN (", 
                             qTerms, ");", sep = ""))
  termsNames <- retVal$term
  names(termsNames) <- retVal$go_id
  
  if(!multipLines) 
    shortNames <- paste(substr(termsNames, 1, numChar),
                        ifelse(nchar(termsNames) > numChar, '...', ''), sep = '')
  else
    shortNames <- sapply(termsNames,
                         function(x) {
                           a <- strwrap(x, numChar)
                           return(paste(a, sep = "", collapse = "\\\n"))
                         })
  
  names(shortNames) <- names(termsNames)
  return(shortNames[whichTerms])
}

# find GO at levels 2 for ONE ontology
nbLevels2 <- function(geneList, ontology){
  myGOdata <- NULL
  try(myGOdata <- new("topGOdata", description="My project", ontology=ontology, 
                      allGenes=geneList,  annot = annFUN.gene2GO, gene2GO = geneID2GO),
      silent = TRUE)
  
  count <- 0
  if (!is.null(myGOdata)){
    # calculate levels for each GO in our data and his ancestors
    levels <- buildLevels(myGOdata@graph)
    
    # select only GO at level 2
    goLevels2 <- mget(ls(levels$nodes2level), envir=levels$nodes2level)==2
    goLevels2 <- names(goLevels2)[goLevels2]
    
    # count genes in each GO
    count <- NULL
    for (i in goLevels2){
      count <- c(count,countGenesInTerm(myGOdata, i))
    }
  }
  return(count)
}

# obtain percent of total genes for each level 2 GO
percentOfGene <- function(geneList, ontology, DefOntology){
  ## Not genes of interest
  All <- nbLevels2(geneList, ontology)/length(geneList)
  
  ## genes of interest
  geneListGoI <- geneList[geneList==1]
  GoI <- nbLevels2(geneListGoI, ontology)/length(geneListGoI)
  
  
  ## percent of gene by GO
  count <- data.frame(name=getTermsDefinition(names(All), ontology, 
                                              numChar = 40), 
                      nbGene=as.vector(All), GoI="All genes", 
                      ontology=DefOntology)
  if (GoI != 0){
    count <- rbind(count,
                   data.frame(name=getTermsDefinition(names(GoI), ontology, 
                                                      numChar = 40), 
                              nbGene=as.vector(GoI), GoI="Genes of interest", 
                              ontology=DefOntology))
  }
  
  ## to obtain the same list of GO between GoI and not GoI, we add line with a percent
  ## of genes equal to 0
  if(length(count$name[!(count$name[count$GoI == "All genes"] %in% 
                         count$name[count$GoI == "Genes of interest"])]) > 0){
    count<-rbind(count,
                 data.frame(name=count$name[!(count$name[count$GoI == "All genes"]
                                              %in% count$name[count$GoI == "Genes of interest"])],
                            nbGene=0, GoI="Genes of interest", ontology=DefOntology))
  }
  
  if(length(count$name[!(count$name[count$GoI == "Genes of interest"] %in% 
                         count$name[count$GoI == "All genes"])]) > 0){
    count<-rbind(count,
                 data.frame(name=count$name[!(count$name[count$GoI == "Genes of interest"] 
                                              %in% count$name[count$GoI == "All genes"])],
                            nbGene=0, GoI="All genes", ontology=DefOntology))
  }
  
  return(count)
}

# recup data topT2 du script de comparaison voulue avec selection des donnees DGE filtrees par logFC et risque alpha
EG.DE.UP <- topT2$logFC > 0
topT2["UP"]<-EG.DE.UP

topT3<-topT2[topT2$UP==TRUE,]

topT3<-topT2[topT2$UP==FALSE,] # dans un second temps

# 
myInterestingGenes2 <- row.names(topT3) # a refaire avec le topT3 down aprs l'etape countUP

geneID2GO <- readMappings(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.topGO.out")
geneNames <- names(geneID2GO)
head(geneNames)

geneList2 <- factor(as.integer(geneNames %in% myInterestingGenes2))
str(geneList2)
names(geneList2) <- geneNames
str(geneList2)

countBP <- percentOfGene(geneList2, "BP", "Biological Process")
countMF <- percentOfGene(geneList2, "MF", "Molecular Function")
countCC <- percentOfGene(geneList2, "CC", "Cellular Component")
count <- rbind(countBP, countMF, countCC)

###
count$GoI <- factor(count$GoI, levels = rev(levels(count$GoI)))

countUP<-count[count$GoI=="Genes of interest",] # selection des gnes DE enrichis up-regulated
countUP["category"]<-"Up genes"

# aprs
countDN<-count[count$GoI=="Genes of interest",] # selection des gnes DE enrichis down-regulated
countDN["category"]<-"Down genes"

# recup des donnees up et down
countTOT<-rbind(countUP, countDN)
head(countTOT)

# specifier le fichier count voulu pour le plot (up, down ou les deux)
ggplot(countTOT, aes(x=as.factor(name), y=nbGene, fill=as.factor(category))) +
  geom_bar(stat="identity", position=position_dodge()) +
  coord_flip() +
  facet_grid( ontology ~ ., scales="free") +
  theme(axis.text.x = element_text(angle = 0, vjust = 1, hjust=1, colour = "black")) +
  scale_fill_discrete(name = "") + ylab(paste0("Percent of gene")) + 
  xlab("GO term")

########################################################################################################################################

##########################################
### Diagramme de Venn                  ###
##########################################

groups <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Design.txt", header=T)
groups <- factor(paste(groups$time,groups$condition,sep="."))
design <- model.matrix(~0+groups)
colnames(design) <- levels(groups)
design
table <- read.table("C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/All_count.txt", header=T, row.names=1)
kable(head(table))
Project = "Laitue"
m <- melt(table)
p <- ggplot(data = m,aes(x=variable, y=log10(value), group=variable, fill=variable))
p <- p + geom_boxplot() + labs(title = paste(Project,"Expression range", sep=" "))
p <- p + theme(axis.text.x = element_text(angle = 90, hjust = 1))
y <- DGEList(counts=table,group=groups)
dim(y)
plotMD(y$counts)
keep<-rowSums(cpm(y)>2)>=3
table(keep)
y<-y[keep,keep.lib.sizes=FALSE]
dim(y)
y <- calcNormFactors(y,method="TMM")
y$samples
dim(y)
y <- estimateGLMCommonDisp(y, design, verbose=TRUE)
y <- estimateGLMTrendedDisp(y, design)
y <- estimateGLMTagwiseDisp(y, design)
plotBCV(y)

fit <- glmFit(y,design)
my.contrasts <- makeContrasts(
  neutral.vs.days27 = day0.neutral - (day2.dark + day2.light + day7.dark + day7.light)/4,
  light.vs.dark = (day2.dark + day7.dark) - (day2.light + day7.light),
  D2D.vs.D0 = day2.dark-day0.neutral,
  D2L.vs.D2D = day2.light-day2.dark,
  D2L.vs.D0 = day2.light-day0.neutral,
  D7L.vs.D2L = day7.light-day2.light,
  D7L.vs.D7D = day7.light-day7.dark,
  D7L.vs.D0 = day7.light-day0.neutral,
  D7D.vs.D0 = day7.dark-day0.neutral,
  D7D.vs.D2D = day7.dark-day2.dark,
  levels=design)

  # appel des packages pour Venn
vennDiagram(objet, 
            include=c("up", "down"),
            counts.col=c("red", "blue"),
            circle.col = c("black")) #objet = sortie de test type decideTests()

### Effet brunissement-conservation
lrt1 <- glmLRT(fit, contrast=my.contrasts[,"D2D.vs.D0"])
topT1 <-as.data.frame(topTags(lrt1,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT1)
a<-topT1[topT1$FDR <0.01,]
dim(a)
topT1<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT1)
topT1$Lsat<-rownames(topT1)
### Effet lumire
lrt2 <- glmLRT(fit, contrast=my.contrasts[,"D2L.vs.D2D"])
topT2 <-as.data.frame(topTags(lrt2,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT2)
a<-topT2[topT2$FDR <0.01,]
dim(a)
topT2<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT2)
topT2$Lsat<-rownames(topT2)
### Effet lumire 2
lrt3 <- glmLRT(fit, contrast=my.contrasts[,"D2L.vs.D0"])
topT3 <-as.data.frame(topTags(lrt3,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT3)
a<-topT3[topT3$FDR <0.01,]
dim(a)
topT3<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT3)
topT3$Lsat<-rownames(topT3)
# tous les genes DE
dev.off()
venn.plot <- venn.diagram(list(topT1$Lsat, topT2$Lsat, topT3$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), 
                          cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Light 2 - Dark 2", "Light 2 - Dark 0"))
grid.draw(venn.plot)

########### J7
### Effet brunissement-conservation  7 jours
lrt4 <- glmLRT(fit, contrast=my.contrasts[,"D7D.vs.D0"])
topT4 <-as.data.frame(topTags(lrt4,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT4)
a<-topT4[topT4$FDR <0.01,]
dim(a)
topT4<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT4)
topT4$Lsat<-rownames(topT4)
### Effet lumire  7 jours
lrt5 <- glmLRT(fit, contrast=my.contrasts[,"D7L.vs.D7D"])
topT5 <-as.data.frame(topTags(lrt5,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT5)
a<-topT5[topT5$FDR <0.01,]
dim(a)
topT5<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT5)
topT5$Lsat<-rownames(topT5)
### Effet lumire bis  7 jours
lrt6 <- glmLRT(fit, contrast=my.contrasts[,"D7L.vs.D0"])
topT6 <-as.data.frame(topTags(lrt6,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT6)
a<-topT6[topT6$FDR <0.01,]
dim(a)
topT6<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT6)
topT6$Lsat<-rownames(topT6)
# tous les genes DE
dev.off()
venn.plot <- venn.diagram(list(topT4$Lsat, topT5$Lsat, topT6$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Dark 7 - Dark 0", "Light 7 - Dark 7", "Light 7 - Dark 0"))
grid.draw(venn.plot)

###############################################################
# que les genes DE up J2
EG.DE.UP <- topT1$logFC > 0
topT1["UP"]<-EG.DE.UP
topT1.UP<-topT1[topT1$UP==TRUE,]
str(topT1.UP)
#
EG.DE.UP <- topT2$logFC > 0
topT2["UP"]<-EG.DE.UP
topT2.UP<-topT2[topT2$UP==TRUE,]
str(topT2.UP)
#
EG.DE.UP <- topT3$logFC > 0
topT3["UP"]<-EG.DE.UP
topT3.UP<-topT3[topT3$UP==TRUE,]
str(topT3.UP)
#
dev.off()
#par(mfrow=c(2,2))
venn.plot <- venn.diagram(list(topT1.UP$Lsat, topT2.UP$Lsat, topT3.UP$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), 
                          cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Light 2 - Dark 2", "Light 2 - Dark 0"))
grid.draw(venn.plot)

###############################################
# que les genes DE up J7
EG.DE.UP <- topT4$logFC > 0
topT4["UP"]<-EG.DE.UP
topT4.UP<-topT4[topT4$UP==TRUE,]
str(topT4.UP)
#
EG.DE.UP <- topT5$logFC > 0
topT5["UP"]<-EG.DE.UP
topT5.UP<-topT5[topT5$UP==TRUE,]
str(topT5.UP)
#
EG.DE.UP <- topT6$logFC > 0
topT6["UP"]<-EG.DE.UP
topT6.UP<-topT6[topT6$UP==TRUE,]
str(topT6.UP)
#
dev.off()
#par(mfrow=c(2,2))
venn.plot <- venn.diagram(list(topT4.UP$Lsat, topT5.UP$Lsat, topT6.UP$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Dark 7 - Dark 0", "Light 7 - Dark 7", "Light 7 - Dark 0"))
grid.draw(venn.plot)

#######################################
# que les genes DE down J2
EG.DE.DN <- topT1$logFC < 0
topT1["DN"]<-EG.DE.DN
topT1.DN<-topT1[topT1$DN==TRUE,]
str(topT1.DN)
#
EG.DE.DN <- topT2$logFC < 0
topT2["DN"]<-EG.DE.DN
topT2.DN<-topT2[topT2$DN==TRUE,]
str(topT2.DN)
#
EG.DE.DN <- topT3$logFC < 0
topT3["DN"]<-EG.DE.DN
topT3.DN<-topT3[topT3$DN==TRUE,]
str(topT3.DN)

################################
# que les genes DE down J7
EG.DE.DN <- topT4$logFC < 0
topT4["DN"]<-EG.DE.DN
topT4.DN<-topT4[topT4$DN==TRUE,]
str(topT4.DN)
#
EG.DE.DN <- topT5$logFC < 0
topT5["DN"]<-EG.DE.DN
topT5.DN<-topT5[topT5$DN==TRUE,]
str(topT5.DN)
#
EG.DE.DN <- topT6$logFC < 0
topT6["DN"]<-EG.DE.DN
topT6.DN<-topT6[topT6$DN==TRUE,]
str(topT6.DN)

#######################
# Graphiques !
dev.off()
venn.plot <- venn.diagram(list(topT1.DN$Lsat, topT2.DN$Lsat, topT3.DN$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), 
                          cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Light 2 - Dark 2", "Light 2 - Dark 0"))
grid.draw(venn.plot)
#################################
# graph up and down 1 contraste
dev.off()
venn.plot <- venn.diagram(list(topT1.UP$Lsat, topT1.DN$Lsat), NULL, fill=c("red", "green"), alpha=c(0.5,0.5), 
                          cex = 2, cat.fontface=4, category.names=c("Dark 2 UP - Dark 0", "Dark 2 DN - Dark 0"))
grid.draw(venn.plot)

###############################
# Gene UP pour comparaison light
dev.off()
venn.plot <- venn.diagram(list(topT2.UP$Lsat, topT5.UP$Lsat), NULL, fill=c("red", "green"), alpha=c(0.5,0.5), cat.dist=0.05, cat.pos=180,
                          cex = 2, cat.fontface="bold", cat.cex= 1.5, category.names=c("Day 2 light - Day 2 dark", "Day 7 light - Day 7 dark"))
grid.draw(venn.plot)

# Gene DN pour comparaison light
dev.off()
venn.plot <- venn.diagram(list(topT2.DN$Lsat, topT5.DN$Lsat), NULL, fill=c("red", "green"), alpha=c(0.5,0.5), cat.dist=0.05, cat.pos=180,
                          cex = 2, cat.fontface="bold", cat.cex= 1.5, category.names=c("Day 2 light - Day 2 dark", "Day 7 light - Day 7 dark"))
grid.draw(venn.plot)


########################################
### Extraction liste genes diag Venn ###
########################################

vennList<-list(topT1$Lsat, topT2$Lsat, topT3$Lsat)
a<-venn(vennList, show.plot=F)
str(a)
inters<-attr(a,"intersections")
lapply(inters,head)

options(max.print=10000000) # aggrandie la limite d'impression dans le fichier texte
setwd("C:/Users/Julie Ripoll/Documents/Lactuca project")
sink(file="GeneListVennDay2.txt", append = F) # debut enregistrement
print(inters) # export de la liste
sink() # fin enregistrement

inters2<-as.data.frame(inters$B)
inters2$Lsat<-inters$B
TopData<-merge(topT2,inters2, by='Lsat',all=F)
str(TopData)
#write.table(TopData,"Data108 Day7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

myInterestingGenes2 <- TopData$Lsat
str(myInterestingGenes2)
geneID2GO <- readMappings(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.topGO.out")
geneNames <- names(geneID2GO)
head(geneNames)
geneList2 <- factor(as.integer(geneNames %in% myInterestingGenes2))
str(geneList2)
names(geneList2) <- geneNames
head(geneList2)
str(geneList2)
summary(geneList2) # pour savoir le nb de genes annotes

Table1 <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/2L vs 2D/TotalData_Term.txt", header=T, sep="\t")
str(Table1)
TopData3<-merge(Table1,TopData, by='Lsat',all=F)
str(TopData3)
write.table(TopData3,"Data280 Day2-2.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

########################################
### Extraction liste genes diag Venn ###
########################################
dev.off()
venn.plot <- venn.diagram(list(topT1.DN$Lsat, topT4.DN$Lsat), NULL, fill=c("red", "green"), alpha=c(0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Day 2 - Day 0", "Day 7 - Day 0"))
grid.draw(venn.plot)
vennList<-list(topT1.UP$Lsat, topT4.UP$Lsat)

vennList<-list(topT4$Lsat, topT5$Lsat, topT6$Lsat)
a<-venn(vennList, show.plot=F)
str(a)
inters<-attr(a,"intersections")
lapply(inters,head)

options(max.print=10000000) # aggrandie la limite d'impression dans le fichier texte
setwd("C:/Users/Julie Ripoll/Documents/Lactuca project")
sink(file="GeneListVennDay7.txt", append = F) # debut enregistrement
print(inters) # export de la liste
sink() # fin enregistrement

inters2<-as.data.frame(inters$B)
inters2$Lsat<-inters$B
TopData<-merge(topT5,inters2, by='Lsat',all=F)
str(TopData)
#write.table(TopData,"Data108 Day7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)

myInterestingGenes2 <- TopData$Lsat
str(myInterestingGenes2)
geneID2GO <- readMappings(file = "C:/Users/Julie Ripoll/Documents/Lactuca project/NGS/Lsat.1.v5.gene.allTranscript.topGO.out")
geneNames <- names(geneID2GO)
head(geneNames)
geneList2 <- factor(as.integer(geneNames %in% myInterestingGenes2))
str(geneList2)
names(geneList2) <- geneNames
head(geneList2)
str(geneList2)
summary(geneList2) # pour savoir le nb de genes annotes

Table1 <- read.table("C:/Users/Julie Ripoll/Documents/Light treatment effect results/7L vs 7D/all gene D7L vs D7D.txt", header=T, sep="\t")
str(Table1)
TopData3<-merge(Table1,TopData, by='Lsat',all=F)
str(TopData3)
write.table(TopData3,"Data108 Day7.txt",dec=".",sep ="\t",quote=FALSE,row.names=FALSE)


################
## pour article 1
lrt8 <- glmLRT(fit, contrast=my.contrasts[,"D7D.vs.D2D"])
topT8 <-as.data.frame(topTags(lrt8,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT8)
a<-topT8[topT8$FDR <0.01,]
dim(a)
topT8<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT8)
topT8$Lsat<-rownames(topT8)
#
EG.DE.UP <- topT8$logFC > 0
topT8["UP"]<-EG.DE.UP
topT8.UP<-topT8[topT8$UP==TRUE,]
str(topT8.UP)
#
EG.DE.DN <- topT8$logFC < 0
topT8["DN"]<-EG.DE.DN
topT8.DN<-topT8[topT8$DN==TRUE,]
str(topT8.DN)
#
dev.off()
venn.plot <- venn.diagram(list(topT1$Lsat, topT4$Lsat, topT8$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Dark 7 - Dark 0", "Dark 7 - Dark 2"))
grid.draw(venn.plot)
dev.off()
venn.plot <- venn.diagram(list(topT1.UP$Lsat, topT4.UP$Lsat, topT8.UP$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Dark 7 - Dark 0", "Dark 7 - Dark 2"))
grid.draw(venn.plot)
dev.off()
venn.plot <- venn.diagram(list(topT1.DN$Lsat, topT4.DN$Lsat, topT8.DN$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Dark 2 - Dark 0", "Dark 7 - Dark 0", "Dark 7 - Dark 2"))
grid.draw(venn.plot)

## pour article 2
### Effet lumire ter
lrt7 <- glmLRT(fit, contrast=my.contrasts[,"D7L.vs.D2L"])
topT7 <-as.data.frame(topTags(lrt7,adjust.method="BH",sort.by="logFC",dim(table)[0]))
dim(topT7)
a<-topT7[topT7$FDR <0.01,]
dim(a)
topT7<-a[a$logFC > 1.5 | a$logFC < -1.5,]
dim(topT7)
topT7$Lsat<-rownames(topT7)
#
EG.DE.UP <- topT7$logFC > 0
topT7["UP"]<-EG.DE.UP
topT7.UP<-topT7[topT7$UP==TRUE,]
str(topT7.UP)
#
EG.DE.DN <- topT7$logFC < 0
topT7["DN"]<-EG.DE.DN
topT7.DN<-topT7[topT7$DN==TRUE,]
str(topT7.DN)

dev.off()
venn.plot <- venn.diagram(list(topT2$Lsat, topT7$Lsat, topT5$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Light 2 - Dark 2", "Light 7 - Light 2", "Light 7 - Dark 7"))
grid.draw(venn.plot)
dev.off()
venn.plot <- venn.diagram(list(topT2.UP$Lsat, topT7.UP$Lsat, topT5.UP$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Light 2 - Dark 2", "Light 7 - Light 2", "Light 7 - Dark 7"))
grid.draw(venn.plot)
dev.off()
venn.plot <- venn.diagram(list(topT2.DN$Lsat, topT7.DN$Lsat, topT5.DN$Lsat), NULL, fill=c("red", "green", "blue"), alpha=c(0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Light 2 - Dark 2", "Light 7 - Light 2", "Light 7 - Dark 7"))
grid.draw(venn.plot)

# test
venn.plot <- venn.diagram(list(topT2.UP$Lsat,topT2.DN$Lsat, topT7.UP$Lsat, topT7.DN$Lsat), NULL, fill=c("red", "red","green","green"), alpha=c(0.5,0.5,0.5,0.5), cex = 2, cat.fontface=4, category.names=c("Light 2 - Dark 2 UP", "Light 2 - Dark 2 DN", "Light 7 - Light 2 UP", "Light 7 - Light 2 DN"))
grid.draw(venn.plot)


###########################################################################
### heat-map sur selection de data (fichier de moyennes par condition)

#data article senescence
data <- read.delim("C:/Users/Julie Ripoll/Documents/Browning effect results/Fichier heatmap text/Hormones.txt", header=T)  
dataS <- read.delim("C:/Users/Julie Ripoll/Documents/Browning effect results/Fichier heatmap text/Senescence.txt", header=T) 
data <- read.delim("C:/Users/Julie Ripoll/Documents/Browning effect results/Fichier heatmap text/Stress_chloro.txt", header=T)  
head(data)
dim(data)

#sens classique
DATA.HEATMAP=as.matrix(data[,-c(1,2,3,7,8,9,10,11,12,13)]) # penser  mettre les bons numeros de colonne  eliminer du data set pour n'avoir que les donnees chiffrees
head(DATA.HEATMAP)
# ou
DATA.HEATMAP=as.matrix(data[,-c(1,2,3,4,7,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24)])
# creation identifiant pour heatmap
data$ID<-interaction(data$Lsat, data$Gene_name) # attention bien avoir les memes noms de colonne
# creation d'une fenetre R qui contiendra le graphique
x11(width=250, height=250)
# Heatmap
heatmap.2 (DATA.HEATMAP,
           # dendrogram control
           Rowv=NULL,
           distfun = dist,
           hclustfun = hclust,
           srtRow=0, 
           adjRow=c(0,0.5), 
           cexRow=0.5, # taille de la police
           srtCol=0, 
           adjCol=c(0.5,0),
           cexCol=0,
           margins=c(3,35), 
           #block separation
           #rowsep=c(16,43,60,91,95,96,97,98), # separateur blanc , verifier numero ligne , hormones
           #rowsep=c(3,5,6,7,9,10,15,17,20,22,24,25,26,30,33,37,39,42,49,58,82,86,91), # separateur blanc , Stress_chloro
		   rowsep=c(8,12,15,16,30,35,46,49,50,56,61,62,69,83,87,91,116,126,134,136,148,160,163), # separateur blanc , senescence
           sepcolor="white",
           sepwidth=c(0.1,0.1),
           # data scaling centrage et reduction
           scale = c("row"),
           na.rm=TRUE,
           #na.color="black",
           # colors
           labRow=data$ID,
           col=redgreen(100),
           trace="none",
           # color key + density info
           density.info="none", # density ou histogram
           key = TRUE, #none si pas d'echelle
           keysize = 0.70,
           key.title = " "
)

###################################################################
### Data article light

#data article light
data <- read.delim("C:/Users/Julie Ripoll/Documents/Light treatment effect results/HeatMapLD/photo_energy_stress.txt", header=T) # margin 
data <- read.delim("C:/Users/Julie Ripoll/Documents/Light treatment effect results/HeatMapLD/Senescence markers.txt", header=T) # margin 
data <- read.delim("C:/Users/Julie Ripoll/Documents/Light treatment effect results/HeatMapLD/Hormones.txt", header=T) # margin 
names(data)
head(data)
dim(data)

#sens classique
DATA.HEATMAP=as.matrix(data[,-c(1,2,3,4,5,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24)]) # penser  mettre les bons numeros de colonne  eliminer du data set pour n'avoir que les donnees chiffrees
head(DATA.HEATMAP)
data$ID<-interaction(data$Lsat, data$Gene_name) # attention bien avoir les memes noms de colonne
library(gplots)
x11(width=250, height=250)
heatmap.2 (DATA.HEATMAP,
           # dendrogram control
           Rowv=NULL, # cluster row
           distfun = dist,
           hclustfun = hclust,
           srtRow=0, 
           adjRow=c(0,0.5), 
           cexRow=0.5, # taille de la police
           srtCol=45, #angle legende bas
           adjCol=c(0.8,0),
           cexCol=0,
           margins=c(3,35), 
           #block separation
           #rowsep=c(5,6,7,10,11,14,16,17,22,23,25,32,34,37,42,43,50,61,68,69,71,72,74,78,79,81,84,85), # blanc, Senescence
           #rowsep=c(3,6,7,10,12,14,18,20,33,51,52,53,56,57,72,73,77,78,80,83,87,88,91,92,96,97,98,99,100,102,105,111), # blanc, Stress
           rowsep=c(8,17,22,41,45), # blanc, Hormones
           sepcolor="white",
           sepwidth=c(0.1,0.1),
           # data scaling centrage et reduction
           scale = c("row"),
           na.rm=TRUE,
           #na.color="black",
           # colors
           labRow=data$ID,
           col=redgreen(100),
           trace="none",
           # color key + density info
           density.info="density",
		   key = TRUE,
           key.title = "Normalized value"
)




################################################
### ACP sur total DATA

# Test
Tmp<-c(0.00,0.00,0.00,22.41,22.41,22.41,23.74,23.74,23.74,5.00,5.00,5.00,10.47,10.47,10.47)
names(Tmp)<-c("day0.neut.1","day0.neut.2","day0.neut.3","day2.dark.1","day2.dark.2 ","day2.dark.3",
               "day7.dark.1", "day7.dark.2", "day7.dark.3", "day2.ligh.1","day2.ligh.2", "day2.ligh.3",
               "day7.ligh.1", "day7.ligh.2", "day7.ligh.3")
x<-merge(Tmp,filteredExpr)

princomp(x, cor = FALSE, scores = TRUE)

pca <- prcomp(t(x), scale = T)
plot(pca)
summary(pca)
loadings <- pca$rotation
scores <- pca$x
plot(scores[,1], scores[,2])
## OK
plot(scores[,1], scores[,2], 
     xlab=paste0("PC1: ", round(summary(pca)$importance[2,1],3)*100, "% variance explained"), 
     ## indicate the % variance explained by PC1
     ylab=paste0("PC2: ", round(summary(pca)$importance[2,2],3)*100, "% variance explained"), 
     ## indicate the % variance explained by PC2
     pch=as.numeric(as.factor(y$samples$group))+15,
     ## points shape according to treatment
     col=myPalette[as.numeric(as.factor(y$samples$group))], 
     ## points color according to susceptibility
     xlim=c(min(scores[,1]), max(scores[,1])) ,
     ylim=c(min(scores[,2]), max(scores[,2])+(max(scores[,2])-min(scores[,2]))/4) 
     ## Let a bit of room on top of the plot for legend
)
## Plot legends
legend("bottomleft", legend=levels(as.factor(y$samples$group)), pch=16:17, col=myPalette[1:5])
text(scores[,1], scores[,2] + 5, y$samples$group)

### autres infos pca
var <- get_pca_var(pca)
var$coord[, 1:4] # dim des Lsat
# Plot the correlation circle
a <- seq(0, 2*pi, length = 100)
plot( cos(a), sin(a), type = 'l', col="gray",
      xlab = "PC1",  ylab = "PC2")
abline(h = 0, v = 0, lty = 2)
# Add active variables
arrows(0, 0, var.coord[, 1], var.coord[, 2], 
       length = 0.1, angle = 15, code = 2)
# Add labels
# Correlation between variables and principal components
var_cor_func <- function(var.loadings, comp.sdev){
  var.loadings*comp.sdev
}
# Variable correlation/coordinates
loadings <- pca$rotation
sdev <- pca$sdev
var.coord <- var.cor <- t(apply(loadings, 1, var_cor_func, sdev))
head(var.coord[, 1:4])
text(var.coord, labels=rownames(var.coord), cex = 1, adj=1)
fviz_pca_var(pca)

var.cos2 <- var.coord^2
head(var.cos2[, 1:4])
fviz_pca_var(pca, col.var="contrib")+
  scale_color_gradient2(low="white", mid="blue", 
                        high="red", midpoint=55) + theme_minimal()

# END