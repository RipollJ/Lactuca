# Lactuca Project

This project has consisted in the analyses of transcriptome of lettuce leaves using the reference genome v5 (published in 2017, https://doi.org/10.1038/ncomms14953) 
and the analyses of physiological data as photosynthesis light curves, chlorophyll a fluorescence, browning etc.

Here, you can found two scripts, provided as is, they were used for a part of analyses of this project.
Transcriptome sequence quality, assembly, annotation and extraction of count table were realized by the GeT-PlaGe core facility (INRA Genotoul, Toulouse, France).

These two scripts are:
 - Transcriptome.r: for Differential Expression Analyses of RNA-seq data
 - Physio.r: for physiological data analyse

For more details on using these scripts, please see the [Wiki](https://gitlab.com/RipollJ/Lactuca/wikis/home) page.

These scripts used several R packages, don't forget to cite them, see [References](https://gitlab.com/RipollJ/Lactuca/wikis/home/references).


--------------------------------------------------------------------
Licence rights: See the [LICENSE](https://gitlab.com/RipollJ/Lactuca/blob/master/License.md) file for license rights and limitations (MIT).
